This project is a mock implementation of a simple e-commerce site using the Open Commerce API.

Technologies Used:

Back End:

	Node.js
	Express
	ejs

Front End:

	jQuery
	Bootstrap
	
Features:

	Browse products by categories and subcategories
	Search bar with automatic product suggestions
	Search page with refinement options to filter and sort results + pagination
	Stores shopping cart session data on server, uses browser cookies to track current cart
	Server-side caching of common API calls
	