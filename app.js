//NPM PACKAGES
var express = require('express');
var request = require('request');
var bodyParser = require('body-parser');
var url = require('url');

//EXPRESS SETTINGS
var app = express();
app.set('view engine', 'ejs');
app.use(express.static('static'));
app.use(bodyParser.json());

//GLOBAL VARIABLES
const BASE_URL = 'http://lyons5.evaluation.dw.demandware.net/s/SiteGenesis//dw/shop/v17_4/';
const CLIENT_ID= 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
const HOME_CATEGORIES = [ 'newarrivals', 'womens', 'mens', 'electronics']; //categories displayed on index
var CACHE = {}; //cache of common OCAPI data
var SESSIONS = {}; //stores session data for each active client


/*******************************/
/***********ROUTES**************/
/*******************************/
app.get('/', (request, response) => {
	startSession(request, response);
	var promises = [];
	var products = {}

	for (i in HOME_CATEGORIES) {
		promises.push(getPromiseOCAPI('product_search/images', {'refine=cgid' : HOME_CATEGORIES[i]}));
	}
	//when all API queries have returned, render the page
	Promise.all(promises).then((res,rej) => {
		for (i in res) {
			var result = JSON.parse(res[i]);
			products[result.selected_refinements.cgid] = result.hits;
		}			
		response.render('index.ejs', {
			'categories' : CACHE.categories,
			'home_categories' : CACHE.home_categories,
			'products' : products
		});
	});
});


app.get('/search', (request, response) => {
	startSession(request, response);
	var queryString = url.parse(request.url,true).query;
	var sort = queryString.sort || "";
	var q = queryString.q || "";
	var count = queryString.count || 25;
	var start = queryString.start || 0;
	var refine = queryString.refine || "";

	var promises = [];
	promises.push(getPromiseOCAPI('product_search', {
		'q' : q,
		refine : refine,
		expand : 'images,prices',
		count : count,
		start : start,
		sort : sort,
		refine : refine
	}));
	promises.push(getPromiseOCAPI('categories/' + refine.split('cgid=')[1]));

	Promise.all(promises).then(function(res,rej) {
		if (rej) console.log("REJECTED");
		var results = JSON.parse(res[0]);
		var category = JSON.parse(res[1]);
		category = category.fault ? null : category;

		response.render('search.ejs', {
			results : results.hits,
			count : count,
			start : start,
			numResults : results.total,
			category : category,
			categories : CACHE.categories });
	});
});

app.post('/searchResults', (req, res) => {
	var url = BASE_URL +'product_search?expand=images,prices&client_id=' + CLIENT_ID + '&' + req.body.q;
	console.log("URL" +url);
	request.get(url, function(error, response, body) {
		res.json(body);
	});

app.post('/getCategoryHeader', (req,res) => {
	var url = BASE_URL +'categories/'+ req.body.category +'?' + 'client_id=' + CLIENT_ID + '&' + req.body.q;
	request.get(url, function(error, response, body) {
		res.json(body);
	});
});

});
app.get('/product', (request, response) => {
	startSession(request, response);
	var id = url.parse(request.url,true).query.id;

	getPromiseOCAPI('products/' + id, {expand : 'images,prices'}).then((res,rej) =>{
		var results = JSON.parse(res);
		response.render('product.ejs', {
			id : results.id,
			name :  results.name,
			image : results.image_groups[0].images[0].link,
			price : results.price,
			description : results.page_description,
			categories : CACHE.categories
		});
	});
});

app.post('/addToCart', (request, response) => {
	var data = request.body;
	var session = SESSIONS[data.session_id];
	session.productsInCart.push(data.product_id);
	session.numProducts++;
	response.json(session);
});

app.post('/suggestions', (request,response) => {
	getPromiseOCAPI('search_suggestion', {q : request.body.q, count : 10})
	.then((res) => {		
		response.json(res)
	});
});

app.get('/cart', (request, response) => {
	var cookies = request.headers.cookie;
	if (!cookies) response.redirect('/');

	var session = SESSIONS[getSessionId(cookies)];
	var ids = session.productsInCart.toString();
	getPromiseOCAPI('products/(' + ids + ')', {expand : 'prices'}).then((res) => {
		response.render('cart.ejs', {
			categories : CACHE.categories,
			products : JSON.parse(res).data,
			numProducts : session.numProducts
		});
	});
});

/*******************************/
/*******HELPER FUNCTIONS********/
/*******************************/

//creates an OCAPI query and returns a promise wrapping the request
function getPromiseOCAPI (resource, params) {
		console.time("API CALL");
	var q = BASE_URL + resource + '?client_id=' + CLIENT_ID;
	if (params) {
		for (p in params) 
			q += '&' + p + '=' + params[p];
	}
	console.time(q +'\n');
	var p = new Promise((resolve, reject) => {		
		request.get(q, function(error, response, body) {
			
			if (error) reject("FAILED!");
			resolve(body);
			console.timeEnd(q + '\n');
		});
	});	
	return p;
}

/*******************************/
/***CACHING-RELATED FUNCTIONS***/
/*******************************/

function getPromiseCategories() {
	return getPromiseOCAPI('categories/root', {levels : 2});
}
function getPromiseHomeCategories() {
	return getPromiseOCAPI('categories/' + '(' + HOME_CATEGORIES.toString() + ')');
}
function cacheCategoryHierarchy() {
	getPromiseCategories().then((resolve) => {
		var categories = {};
		var results = JSON.parse(resolve).categories;
		for (i in results) {
			var subCategories = {};
			for (j in results[i].categories) {
				subCategories[results[i].categories[j].id] = results[i].categories[j].name;
			}
			categories[results[i].id] = {
				'name'	: results[i].name,
				'subCategories' : subCategories
			}
		}
		CACHE.categories = categories;
	});	
}
function cacheHomeCategoryData() {
	getPromiseHomeCategories().then((resolve) => {
		var home_categories = {};
		var data = JSON.parse(resolve).data;
		for (i in data) {
			home_categories[data[i].id] = data[i];
			home_categories[data[i].id].c_headerMenuBanner = 
				home_categories[data[i].id].c_headerMenuBanner.split('src="')[1].split('"')[0];
		}
		CACHE.home_categories = home_categories
	});		
}


/*******************************/
/***SESSION-RELATED FUNCTIONS***/
/*******************************/

//not enforcing unique, but chance of id collision between two clients is [1.2 * 10^-36]
function generateSessionId() {
	var text = '';
	var charSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for (let i = 0; i < 20; i++)
		text += charSet.charAt(Math.floor(Math.random() * charSet.length));
	return text;
}
//if client has no session id cookie, create and initialize a new session
function startSession(request, response) {
	if (!request.headers.cookie) {
		var sessionId = generateSessionId();
		SESSIONS[sessionId] = {id : sessionId, productsInCart : [], numProducts : 0};
		response.set('Set-Cookie', "session=" + sessionId);
	}	
}
//parse request cookies and extract data
function getSessionId(cookies) {
	return  cookies.replace(/(?:(?:^|.*;\s*)session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
}
function getNumInCart(cookies) {
	return cookies.replace(/(?:(?:^|.*;\s*)num\s*\=\s*([^;]*).*$)|^.*$/, "$1");	 			
}


app.listen(8888, function() {
	cacheCategoryHierarchy();
	cacheHomeCategoryData();	
});